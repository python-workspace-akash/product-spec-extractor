# -*- coding: utf-8 -*-
"""
Created on Wed Apr 22 12:19:28 2020
Extract content from pdf file
@author: Avinash
"""

import PyPDF2



def extractContent(page):
    pageText = page.extractText()
    pageText = cleanPage(pageText)
    
    
    #add to output dictionary

def cleanPage(pageText):
    lines = pageText.split('\n')
    print('Number of lines: ' , len(lines))
    cleaned = list()
    for line in lines:
        if line.strip() != '':
            cleaned.append(line)
            pass
        pass
    return cleaned
        

def extractFromPdf(filePath):
    file = open(filePath, 'rb')

    # creating a pdf reader object
    fileReader = PyPDF2.PdfFileReader(file)
    
    # print the number of pages in pdf file
    print("Number of pages : " , fileReader.numPages)
    extractResult = {}
    for page in fileReader.pages:
        extractContent(page)
        #break to stop after page 1 
        break
    

def __main__():
    # creating an object 
    extractFromPdf('data/SDM Spec Form - Foundation + Strobe Sticks.pdf')
    

#starting running from here
__main__()





    

