**Product Specification Extraction Project from PDF or DOC File Formats**

This project is to extract Product Specification Sheet into Excel
This Sheet can be either of pdf or doc file format
---

## Project Setup

Before running you need to have python 3 installed on your system 
you can download python , prefer anaconda python 3 from the below website
https://repo.anaconda.com/archive/Anaconda3-2020.02-Windows-x86_64.exe

After installation you need to install two python libraries
using following commands 

PyPDF2 : https://pypi.org/project/PyPDF2/
> `pip install PyPDF2 `

Tika: https://github.com/chrismattmann/tika-python
> `pip install tika`


NOTE: While running extractor-doc for first time it takes some time, since it will be downloading few jar files related to Word Document Extraction

Now you will be ready to run the .py files


---

## Running the project
You can open python files with any editor "Spyder" is installed with python
which is useful for python coding

You can open extractor-doc.py file in Spyder and there is Run icon on it.
You can click on it and program runs and you can see the output 
on the right side of the editor

## Running from command line
Open command prompt in windows 
navigate to project folder and 
run the command

> `python extractor-doc.py`  
> or  
> `python extractor-pdf.py`

