# -*- coding: utf-8 -*-
"""
Created on Sun Apr 26 00:03:27 2020
Extract content from word document

@author: Avinash
"""

from tika import parser
import re,json

"""
All the patterns of each field can be written in below dictionary
"""
patterns = {}


"""
Load all the patterns from the json file and put it in the patterns dictionary
"""
def loadPatterns():
    global patterns
    with open('patterns-doc.txt') as json_file:
        data = json.load(json_file)
        patterns = data
        print('Patterns loaded successfully...')
        pass
    pass

loadPatterns()
print(patterns)
    

"""
Cleaning value logic can be written here 
This can be common for all the values
Currently it will remove spaces or tabs left side or right side of string
"""
def cleanVal(value):
    return value.strip()

def extract(data):
    global patterns
    #get UPC ids
    #result = re.findall(patterns['section0']['upc'],data)
    for section in patterns.keys():
        for field in patterns[section].keys():
            pattern = patterns[section][field]
            if pattern == '':
                continue
            result = re.findall(pattern,data)
            print(result)
            for match in result :
                print(field,' : ',cleanVal(match))
                pass
            pass
        pass
    pass

   
    

def extractFromWordDoc(filePath) :
    data = parser.from_file(filePath,xmlContent = False)
    #print(data['metadata'])
    #print(data['content'])
    print('******************Extraction Started ***********************')
    extract(data['content'])
    
    

def __main__():
    # creating an object 
    extractFromWordDoc('data/Hybrid Spec Form Nasal Rinse.doc')
    

#starting running from here
__main__()
